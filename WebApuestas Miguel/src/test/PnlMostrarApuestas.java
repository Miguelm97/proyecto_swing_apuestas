package test;

import excepciones.ApuestasException;
import java.awt.BorderLayout;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import modelo.dao.ApuestasControlerImpl;
import modelo.dao.UsuariosControlerImpl;
import modelo.entidades.Apuesta;

public class PnlMostrarApuestas {

    public static JPanel crearPanel() throws ApuestasException {

        PnlCrearApuesta pnlCrearApuesta = new PnlCrearApuesta();
        PnlModificarApuesta pnlModificarApuesta = new PnlModificarApuesta();

        JTable tablaApuestas;                       //TABLA EN LA QUE SE MUESTRAN LAS APUESTAS
        TableModel model = new TableModel();
        tablaApuestas = new JTable(model);

        formatoTabla(tablaApuestas);

        ApuestasControlerImpl bbdd = new ApuestasControlerImpl();
        UsuariosControlerImpl bbddUsu = new UsuariosControlerImpl();

        JButton btnCrear = new JButton("CREAR"),
                btnBorrar = new JButton("BORRAR"),
                btnModificar = new JButton("MODIFICAR"),
                btnResultado = new JButton("RESULTADO");

        btnCrear.addActionListener(e -> {
            FramePrincipal.cambiarPanel(pnlCrearApuesta.crearPanel());
        });
        btnBorrar.addActionListener(e -> {
            
            int[] indFil = tablaApuestas.getSelectedRows(); //ARRAY DE FILAS SELECCIONADAS
            int id;
            
            if (indFil.length > 0) {
                if (JOptionPane.showConfirmDialog(null, "¿Está seguro de que desea eliminar esta apuesta?", "Eliminar apuesta", JOptionPane.YES_NO_OPTION)
                        == JOptionPane.YES_OPTION) {            //CONFIRMACIÓN PARA ELIMINAR LA APUESTA
                    for (int i = 0; i < indFil.length; i++) {
                        id = Integer.parseInt(tablaApuestas.getValueAt(indFil[i], 0) + ""); //ID DE CADA FILA
                        try {
                            bbdd.deleteApuesta(id); //LLAMO AL MÉTODO QUE BORRA LA APUESTA DE LA BBDD                            
                        } catch (ApuestasException ex) {
                            Logger.getLogger(PnlMostrarApuestas.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    JOptionPane.showMessageDialog(null, "Las apuestas han sido eliminadas", "Eliminar apuesta", JOptionPane.INFORMATION_MESSAGE);
                    
                    /*                  //BORRA LAS FILAS (NO FUNCIONA)
                    for (int i = indFil.length - 1; i == 0; i--) {
                        model.deleteRow(indFil[i])   ;
                        System.out.println("borrar fila " + indFil[i]);
                    }*/
                    
                    try {           //ACTUALIZA LA TABLA
                        tablaApuestas.setModel(new TableModel());
                        formatoTabla(tablaApuestas);
                    } catch (ApuestasException ex) {
                        Logger.getLogger(PnlMostrarApuestas.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    
                }
            } else {
                JOptionPane.showMessageDialog(null, "Ninguna fila ha sido seleccionada", "Eliminar apuesta", JOptionPane.ERROR_MESSAGE);
            }
            
        });
        btnModificar.addActionListener(e -> {
            
            int indiceFila = tablaApuestas.getSelectedRow();    //OBTENGO LA FILA SELECCIONADAD
            if (indiceFila >= 0) {
                int id = (int) tablaApuestas.getValueAt(indiceFila, 0); //OBTENGO EL ID DE ESA FILA

                try {
                    Apuesta apuesta = bbdd.getApuesta(id);  //OBTENGO LA APUESTA A LA QUE LE CORRESPONDE ESE ID
                    FramePrincipal.cambiarPanel(pnlModificarApuesta.crearPanel());  //MUESTRO EL PANER MODIFICAR
                    pnlModificarApuesta.rellenar(apuesta);  //CARGO LOS DATOS EN EL PANEL
                } catch (ApuestasException ex) {
                    Logger.getLogger(PnlMostrarApuestas.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                JOptionPane.showMessageDialog(null, "Ninguna fila ha sido seleccionada", "Eliminar apuesta", JOptionPane.ERROR_MESSAGE);
            }

        });

        btnResultado.addActionListener(e -> {   //AÑADIR UN RESULTADO A LAS FILAS SELECCIONADAS
            
            int[] indFil = tablaApuestas.getSelectedRows(); //ARRAY DE FILAS SELECCIONADAS
            int id;

            try {
                int dialogResult = JOptionPane.showConfirmDialog(null, "¿El pronóstico ha sido acertado?", "Resultado apuesta", JOptionPane.YES_NO_CANCEL_OPTION);
                if (dialogResult == JOptionPane.YES_OPTION) {            //INDICA RESULTADO DE LA APUESTA
                    for (int i = 0; i < indFil.length; i++) {
                        id = Integer.parseInt(tablaApuestas.getValueAt(indFil[i], 0) + ""); //ID DE CADA FILA
                        bbdd.resultadoApuesta(id, 1); //PRONOSTICO CORRECTO
                        bbddUsu.actualizarSaldo(id);
                    }
                } else if (dialogResult == JOptionPane.NO_OPTION) {

                    for (int i = 0; i < indFil.length; i++) {
                        id = Integer.parseInt(tablaApuestas.getValueAt(indFil[i], 0) + "");
                        bbdd.resultadoApuesta(id, 0); //PRONOSTICO INCORRECTO
                    }

                }

                        //ACTUALIZA LA TABLA
                tablaApuestas.setModel(new TableModel());
                formatoTabla(tablaApuestas);

            } catch (ApuestasException ex) {
                Logger.getLogger(PnlMostrarApuestas.class.getName()).log(Level.SEVERE, null, ex);
            }

        });

        JPanel botonera = new JPanel();
        botonera.add(btnCrear);
        botonera.add(btnBorrar);
        botonera.add(btnModificar);
        botonera.add(btnResultado);

        JPanel panel = new JPanel(new BorderLayout(5, 5));
        panel.setBorder(BorderFactory.createEmptyBorder(8, 20, 8, 20));

        panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        panel.add(new JScrollPane(tablaApuestas), BorderLayout.CENTER);
        panel.add(botonera, BorderLayout.SOUTH);

        return panel;

    }

    private static void formatoTabla(JTable tablaApuestas) {
        tablaApuestas.getTableHeader().setReorderingAllowed(false);
        tablaApuestas.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaApuestas.getColumnModel().getColumn(0).setMinWidth(0);
        tablaApuestas.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaApuestas.getColumnModel().getColumn(1).setPreferredWidth(40);
        tablaApuestas.getColumnModel().getColumn(3).setPreferredWidth(200);
        tablaApuestas.getColumnModel().getColumn(5).setPreferredWidth(30);
    }

}

class TableModel extends AbstractTableModel {

    private final List<Apuesta> datos;

    public TableModel() throws ApuestasException {
        this.datos = new ApuestasControlerImpl().getApuestas();
    }

    //Número de filas del modelo
    @Override
    public int getRowCount() {
        return datos.size();
    }

    //Número de columnas del modelo
    @Override
    public int getColumnCount() {
        return 6;
    }

    //Añadir un nuevo elemento
    public void addRow(Apuesta p) {
        // Recuperar indice elemento nuevo
        int nuevoIndice = datos.size();
        datos.add(p);

        // Notificar (para actualizacion visual de la tabla) que se ha 
        // insertado una nueva fila
        fireTableRowsInserted(nuevoIndice, nuevoIndice);
    }

    //Eliminar un elemento por su indice
    public void deleteRow(int indiceFila) {
        // Recuperar indice elemento nuevo
        if (indiceFila >= 0 && indiceFila < datos.size()) {
            datos.remove(indiceFila);

            // Notificar (para actualizacion visual de la tabla) que se ha 
            // eliminado una fila
            fireTableRowsDeleted(indiceFila, indiceFila);
        }
    }

    //Devolver el valor de la celda que correspnde a la posicion fila y columnas
    //de los argumentos
    @Override
    public Object getValueAt(int fila, int columna) {
        switch (columna) {
            case 0:
                return datos.get(fila).getIdApuesta();
            case 1:
                return datos.get(fila).getDeporte();
            case 2:
                return datos.get(fila).getEqLocal() + " - " + datos.get(fila).getEqVisitante();
            case 3:
                return datos.get(fila).getMercado();
            case 4:
                return datos.get(fila).getFechaPartido();
            case 5:
                if (datos.get(fila).getAcertada() == 1) {
                    return "SI";
                } else if (datos.get(fila).getAcertada() == 0) {
                    return "NO";
                } else if (datos.get(fila).getAcertada() == -1) {
                    return "...";
                } else {
                    return "ERROR";
                }

            default:
                return null;
        }
    }

    //Devolver nombre de la columna por si indice
    @Override
    public String getColumnName(int columna) {
        switch (columna) {
            case 0:
                return "Id";
            case 1:
                return "Deporte";
            case 2:
                return "Equipos";
            case 3:
                return "Pronóstico";
            case 4:
                return "Fecha Partido";
            case 5:
                return "Acertado";
            default:
                return null;
        }
    }

    //Devolver el tipo de dato de la columna por si indice
    @Override
    public Class<?> getColumnClass(int columna) {
        switch (columna) {
            case 0:
                return Integer.class;
            case 1:
                return String.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
            case 4:
                return String.class;
            case 5:
                return String.class;
            default:
                return null;
        }
    }

    //Por defecto el JTable es de solo lectura. Este método devuelve true 
    //para las celdas (fila, columna) que quiero se puedan modificar
    //Todas las celdas editables
    @Override
    public boolean isCellEditable(int fila, int columna) {
        return true;
    }

}
