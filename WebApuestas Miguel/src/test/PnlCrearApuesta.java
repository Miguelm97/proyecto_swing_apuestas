
package test;

import excepciones.ApuestasException;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import modelo.dao.ApuestasControlerImpl;
import modelo.entidades.Apuesta;

public class PnlCrearApuesta {  //INTERFAZ GRÁFICA
    
    private JTextField txtDeporte, txtEqLocal, txtEqVisitante, txtMercado, txtCuota, txtFechaPartido;
    
    public JPanel crearPanel() {
        JButton 
                btnAtras = new JButton("Atras"),
                btnAceptar = new JButton("AÑADIR");
        
        JLabel 
                lblTitulo = new JLabel("Añadir apuesta"),
                lblDeporte = new JLabel("Deporte"),
                lblEqLocal = new JLabel("Equipo local"),
                lblEqVisitante = new JLabel("Equipo Visitante"),
                lblMercado = new JLabel("Pronóstico"),
                lblCuota = new JLabel("Cuota"),
                lblFechaPartido = new JLabel("Fecha partido");
        

        lblTitulo.setHorizontalAlignment(JLabel.CENTER);
        lblTitulo.setFont(new Font("", Font.PLAIN, 18));
        
        txtDeporte = new JTextField();
        txtEqLocal = new JTextField();
        txtEqVisitante = new JTextField();
        txtMercado = new JTextField();
        txtCuota = new JTextField();
        txtFechaPartido = new JTextField();               
        
        btnAceptar.addActionListener(e -> {   //BOTON AÑADIR      
            aniadirApuesta();       
        });
        btnAtras.addActionListener(e -> {   //BOTON PARA VOLVER
            try {
                FramePrincipal.cambiarPanel(PnlMostrarApuestas.crearPanel());   //VUELVE AL PANEL PRINCIPAL
            } catch (ApuestasException ex) {
                Logger.getLogger(PnlCrearApuesta.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        JPanel datos = new JPanel(new GridLayout(6, 0 , 5,5));
        datos.add(lblDeporte);
        datos.add(txtDeporte);
        datos.add(lblEqLocal);
        datos.add(txtEqLocal);
        datos.add(lblEqVisitante);
        datos.add(txtEqVisitante);
        datos.add(lblFechaPartido);
        datos.add(txtFechaPartido);
        datos.add(lblMercado);
        datos.add(txtMercado);
        datos.add(lblCuota);
        datos.add(txtCuota);
        
        JPanel botonera = new JPanel();
        botonera.add(btnAtras);
        botonera.add(btnAceptar);

        
        JPanel panel = new JPanel(new BorderLayout(5,5));
        panel.setBorder(BorderFactory.createEmptyBorder(8, 20, 8, 20));

        panel.add(datos, BorderLayout.NORTH);
        panel.add(botonera, BorderLayout.SOUTH);
        
        return panel;
        
    }

    private void aniadirApuesta() throws NumberFormatException, HeadlessException { //FUNCION PARA AÑADIR APUESTA
        
        //CREACIÓN DE VARIABLES
        String
                deporte = txtDeporte.getText(),
                fechaPartido = txtFechaPartido.getText(),
                eqLocal = txtEqLocal.getText(),
                eqVisitante = txtEqVisitante.getText(),
                mercado = txtMercado.getText(),
                cuota = txtCuota.getText();
        
        //CONTROL DE POSIBLES FALLOS
        if(!(deporte.isEmpty() || fechaPartido.isEmpty() || eqLocal.isEmpty()
                || eqVisitante.isEmpty() || mercado.isEmpty() || cuota.isEmpty())){ //Comprobación de que ningun campo está vacio
            
            if(esNumero(cuota)){//Comprobación de que cuota sea un valor numérico
                
                Apuesta apu = new Apuesta(0, deporte, fechaPartido, eqLocal, eqVisitante, mercado,  Double.parseDouble(cuota), -1); //RELLENO OBJETO APUESTA
                
                ApuestasControlerImpl bbdd = new ApuestasControlerImpl(); //LLAMO A LA BBDD
                
                try {
                    bbdd.addApuesta(apu);   //METODO PARA AÑADIR APUESTA A LA BBDD
                } catch (ApuestasException ex) {
                    Logger.getLogger(PnlCrearApuesta.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                JOptionPane.showMessageDialog(null, "Apuesta creada correctamente", "Crear apuesta", JOptionPane.INFORMATION_MESSAGE);
                
                limpiar();                 
                
            } else{
                JOptionPane.showMessageDialog(null, "La cuota debe ser un valor numérico", "Crear apuesta", JOptionPane.ERROR_MESSAGE);
            }
            
        } else{
            JOptionPane.showMessageDialog(null, "Rellene todos los campos", "Crear apuesta", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void limpiar() {    //VACÍA TODOS LOS CAMPOS
        txtDeporte.setText("");
        txtFechaPartido.setText("");
        txtEqLocal.setText("");
        txtEqVisitante.setText("");
        txtMercado.setText("");
        txtCuota.setText("");
    }
    
    private static boolean esNumero(String cadena){ //COMPRUEBA SI UN VALOR ES UN NÚMERO
	try {
		Double.parseDouble(cadena);
		return true;
	} catch (NumberFormatException nfe){
		return false;
	}
}
}
   