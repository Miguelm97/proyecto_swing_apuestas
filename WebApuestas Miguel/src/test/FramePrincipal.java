/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.curso.swing.Ventana;
import excepciones.ApuestasException;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Grupo 1 Java
 */
public class FramePrincipal{
    
    static JFrame principalFrame = Ventana.crear("PRINCIPAL", 600, 400, false);
    public static void crearFramePrincipal() throws ApuestasException {
        
        JLabel lblTitulo = new JLabel("CASH 24");
        lblTitulo.setHorizontalAlignment(JLabel.CENTER);
        lblTitulo.setFont(new Font("Serif", Font.PLAIN, 20));
        principalFrame.add(lblTitulo, BorderLayout.NORTH);
        
        principalFrame.setLocationByPlatform(true);
        cambiarPanel(PnlMostrarApuestas.crearPanel());    
    }

    private static JPanel p = null;
    public static void cambiarPanel(JPanel panel){
        
        if(p != null){
            principalFrame.remove(p);
        }
        
        principalFrame.add(panel, BorderLayout.CENTER);
        principalFrame.setVisible(true);
        p = panel;

    }

}
