
package test;

import excepciones.ApuestasException;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import modelo.dao.ApuestasControlerImpl;
import modelo.entidades.Apuesta;

public class PnlModificarApuesta {
    
    private JTextField txtDeporte, txtEqLocal, txtEqVisitante, txtMercado, txtCuota, txtFechaPartido;
    private int id;
    
    public JPanel crearPanel() {
        JButton 
                btnAtras = new JButton("Atras"),
                btnAceptar = new JButton("MODIFICAR");
        
        JLabel 
                lblDeporte = new JLabel("Deporte"),
                lblEqLocal = new JLabel("Equipo local"),
                lblEqVisitante = new JLabel("Equipo Visitante"),
                lblMercado = new JLabel("Pronóstico"),
                lblCuota = new JLabel("Cuota"),
                lblFechaPartido = new JLabel("Fecha partido");
        

        txtDeporte = new JTextField();
        txtEqLocal = new JTextField();
        txtEqVisitante = new JTextField();
        txtMercado = new JTextField();
        txtCuota = new JTextField();
        txtFechaPartido = new JTextField();               
        
        btnAceptar.addActionListener(e -> {         
            try {       
                modifApuesta();
            } catch (NumberFormatException ex) {
                Logger.getLogger(PnlModificarApuesta.class.getName()).log(Level.SEVERE, null, ex);
            } catch (HeadlessException ex) {
                Logger.getLogger(PnlModificarApuesta.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ApuestasException ex) {
                Logger.getLogger(PnlModificarApuesta.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        btnAtras.addActionListener(e -> {
            try {
                FramePrincipal.cambiarPanel(PnlMostrarApuestas.crearPanel());   //VUELVE AL PANEL PRINCIPAL
            } catch (ApuestasException ex) {
                Logger.getLogger(PnlModificarApuesta.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        JPanel datos = new JPanel(new GridLayout(6, 0 , 5,5));
        datos.add(lblDeporte);
        datos.add(txtDeporte);
        datos.add(lblEqLocal);
        datos.add(txtEqLocal);
        datos.add(lblEqVisitante);
        datos.add(txtEqVisitante);
        datos.add(lblFechaPartido);
        datos.add(txtFechaPartido);
        datos.add(lblMercado);
        datos.add(txtMercado);
        datos.add(lblCuota);
        datos.add(txtCuota);
        
        JPanel botonera = new JPanel();
        botonera.add(btnAtras);
        botonera.add(btnAceptar);

        
        JPanel panel = new JPanel(new BorderLayout(5,5));
        panel.setBorder(BorderFactory.createEmptyBorder(8, 20, 8, 20));

        panel.add(datos, BorderLayout.NORTH);
        panel.add(botonera, BorderLayout.SOUTH);
        
        return panel;
        
    }

    private void modifApuesta() throws NumberFormatException, HeadlessException, ApuestasException {    //FUNCIÓN PARA MODIFICAR LA APUESTA
        
        //CREACIÓN DE VARIABLES
        String
                deporte = txtDeporte.getText(),
                fechaPartido = txtFechaPartido.getText(),
                eqLocal = txtEqLocal.getText(),
                eqVisitante = txtEqVisitante.getText(),
                mercado = txtMercado.getText(),
                cuota = txtCuota.getText();
        
        //CONTROL DE POSIBLES FALLOS
        if(!(deporte.isEmpty() || fechaPartido.isEmpty() || eqLocal.isEmpty()
                || eqVisitante.isEmpty() || mercado.isEmpty() || cuota.isEmpty())){ //Comprobación de que ningun campo está vacio
            
            if(esNumero(cuota)){    //Comprobación de que cuota sea un valor numérico
                
                Apuesta apu = new Apuesta(id, deporte, fechaPartido, eqLocal, eqVisitante, mercado,  Double.parseDouble(cuota), -1);    //RELLENO OBJETO APUESTA
                
                ApuestasControlerImpl bbdd = new ApuestasControlerImpl();   //LLAMO A LA BBDD
                
                try {
                    bbdd.editApuesta(apu);   //METODO PARA MODIFICAR APUESTA A LA BBDD
                } catch (ApuestasException ex) {
                    Logger.getLogger(PnlModificarApuesta.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                JOptionPane.showMessageDialog(null, "Apuesta modificada correctamente", "Crear apuesta", JOptionPane.INFORMATION_MESSAGE);  
                FramePrincipal.cambiarPanel(PnlMostrarApuestas.crearPanel());   //VUELVE AL PANEL PRINCIPAL
                
            } else{
                JOptionPane.showMessageDialog(null, "La cuota debe ser un valor numérico", "Crear apuesta", JOptionPane.ERROR_MESSAGE);
            }
            
        } else{
            JOptionPane.showMessageDialog(null, "Rellene todos los campos", "Crear apuesta", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void limpiar() {    //VACÍA TODOS LOS CAMPOS
        txtDeporte.setText("");
        txtFechaPartido.setText("");
        txtEqLocal.setText("");
        txtEqVisitante.setText("");
        txtMercado.setText("");
        txtCuota.setText("");
    }
    
    public void rellenar(Apuesta apu) { //RELLENA TODOS LOS CAMPOS CON LOS DATOS DEL OBJETO APUESTA
        txtDeporte.setText(apu.getDeporte() + "");
        txtFechaPartido.setText(apu.getFechaPartido() + "");
        txtEqLocal.setText(apu.getEqLocal() + "");
        txtEqVisitante.setText(apu.getEqVisitante() + "");
        txtMercado.setText(apu.getMercado() + "");
        txtCuota.setText(apu.getCuota() + "");
        id = apu.getIdApuesta();
    }
    
    private static boolean esNumero(String cadena){ //COMPRUEBA SI UN VALOR ES UN NÚMERO
	try {
		Double.parseDouble(cadena);
		return true;
	} catch (NumberFormatException nfe){
		return false;
	}
}
}
   