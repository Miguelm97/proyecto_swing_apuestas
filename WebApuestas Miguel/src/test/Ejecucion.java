/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import com.curso.swing.Ventana;
import excepciones.ApuestasException;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import modelo.dao.UsuariosControlerImpl;
import modelo.entidades.Usuario;

/**
 *
 * @author Grupo 1 Java
 */
public class Ejecucion {

    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("No se pueden cargar los drivers de base de datos");
            System.exit(0);
        }
        SwingUtilities.invokeLater(() -> new Ejecucion().startup());
    }

    private JFrame loginFrame = Ventana.crear("LOGIN", 250, 250, false);
    private JFrame registrarFrame = Ventana.crear("REGISTRAR", 250, 250, false);
    
    private JTextField txtUsuario, txtUsuarioReg;
    private JPasswordField pwContrasenia, pwContraseniaReg, pwConfContrasenia;

    private void startup() {        //INTERFAZ GRÁFICA DE LA VENTANA DE LOGIN

        registrarFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        
        txtUsuario = new JTextField("m");
        pwContrasenia = new JPasswordField("m");

        JLabel lblContrasenia = new JLabel("Contrasenia"),
                lblUuario = new JLabel("Usuario"),
                lblTitulo = new JLabel("LOGIN");

        JButton btnEntrar = new JButton("ENTRAR"),
                btnCrear = new JButton("NUEVO");

        btnEntrar.addActionListener(e -> {                  
            entrar();
        });
        btnCrear.addActionListener(e -> {
            registrarse();
        });

        lblTitulo.setHorizontalAlignment(JLabel.CENTER);
        lblTitulo.setFont(new Font("Serif", Font.PLAIN, 20));

        JPanel datos = new JPanel(new GridLayout(4, 0, 5, 5));
        datos.add(lblUuario);
        datos.add(txtUsuario);
        datos.add(lblContrasenia);
        datos.add(pwContrasenia);

        JPanel botonera = new JPanel();
        botonera.add(btnEntrar);
        botonera.add(btnCrear);

        JPanel panel = new JPanel(new BorderLayout(5, 5));
        panel.setBorder(BorderFactory.createEmptyBorder(8, 20, 8, 20));

        panel.add(lblTitulo, BorderLayout.NORTH);
        panel.add(datos, BorderLayout.CENTER);
        panel.add(botonera, BorderLayout.SOUTH);

        loginFrame.setContentPane(panel);
        loginFrame.setLocationByPlatform(true);
        loginFrame.setVisible(true);

    }

    public void registrarse() { //INTERFAZ GRÁFICA DE LA VENTANA DE REGISTRARSE

        txtUsuarioReg = new JTextField();
        
        pwContraseniaReg = new JPasswordField();
        pwConfContrasenia = new JPasswordField();

        JLabel lblContrasenia = new JLabel("Contraseña"),
                lblUuario = new JLabel("Usuario"),
                lblConfContrasenia = new JLabel("Confirmar contrasenia"),
                lblTitulo = new JLabel("CREAR ADMIN");

        JButton btnEntrar = new JButton("CONTINUAR"),
                btnVolver = new JButton("Volver");

        btnVolver.addActionListener(e -> {
            registrarFrame.setVisible(false);
        });

        btnEntrar.addActionListener(e -> {                      
            registrar();
        });

        lblTitulo.setHorizontalAlignment(JLabel.CENTER);
        lblTitulo.setFont(new Font("Serif", Font.PLAIN, 20));

        JPanel datos = new JPanel(new GridLayout(6, 0, 5, 5));
        datos.add(lblUuario);
        datos.add(txtUsuarioReg);
        datos.add(lblContrasenia);
        datos.add(pwContraseniaReg);
        datos.add(lblConfContrasenia);
        datos.add(pwConfContrasenia);

        JPanel botonera = new JPanel();
        botonera.add(btnEntrar);
        botonera.add(btnVolver);

        JPanel panel = new JPanel(new BorderLayout(5, 5));
        panel.setBorder(BorderFactory.createEmptyBorder(8, 20, 8, 20));

        panel.add(lblTitulo, BorderLayout.NORTH);
        panel.add(datos, BorderLayout.CENTER);
        panel.add(botonera, BorderLayout.SOUTH);

        registrarFrame.setContentPane(panel);
        registrarFrame.setLocationByPlatform(true);
        registrarFrame.setVisible(true);
    }

    
    private void entrar() throws HeadlessException {    //FUNCION PARA LOGARSE
        
        //CREACIÓN DE VARIABLES
        String alias, password;
        alias = txtUsuario.getText();
        char[] arrayPassword = pwContrasenia.getPassword();
        password = new String(arrayPassword);
        UsuariosControlerImpl bbdd = new UsuariosControlerImpl();
        Usuario usu = new Usuario(alias, password, 1);  //RELLENA EL CONSTRUCTOR
        
        //CONTROL DE POSIBLES FALLOS
        try {
            if(!alias.isEmpty() && !password.isEmpty()){    //Comprobación de que ningun campo está vacio
                if ((bbdd.getUsuario(alias).getAdmin()) != -1) {    //Comprobación de que el usuario existe
                    if((bbdd.getUsuario(alias).getAdmin()) == 1){
                        if (bbdd.getUsuario(alias).getPassword().equals(usu.getPassword())) {   //Comprobación de que la contraseña es correcta
                            FramePrincipal.crearFramePrincipal();   //TE LLEVA A LA VENTANA PRINCIPAL
                            loginFrame.setVisible(false);
                        } else {
                            JOptionPane.showMessageDialog(null, "Contraseña incorrecta", "Login", JOptionPane.ERROR_MESSAGE); //Salta esto pero debería estar bien
                        }
                    } else{
                        JOptionPane.showMessageDialog(null, "Usuario no tiene permisos de administrador", "Login", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Usuario no existe", "Login", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Rellene todos los campos", "Login", JOptionPane.ERROR_MESSAGE);
            }
        } catch (ApuestasException ex) {
            Logger.getLogger(Ejecucion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void registrar() throws HeadlessException { //FUNCION PARA REGISTRARSE
        
        //CREACIÓN DE VARIABLES
        String alias, password, confPassword;
        alias = txtUsuarioReg.getText();
        char[] arrayPassword = pwContraseniaReg.getPassword();
        password = new String(arrayPassword);
        arrayPassword = pwConfContrasenia.getPassword();
        confPassword = new String(arrayPassword);
        
        //CONTROL DE POSIBLES FALLOS
        if(!alias.isEmpty() && !password.isEmpty() && !confPassword.isEmpty()){ //Comprobación de que ningun campo está vacio
            if (password.equals(confPassword)) {    //Comprobación de que las contraseñas coinciden
                
                UsuariosControlerImpl bbdd = new UsuariosControlerImpl();
                Usuario usu = new Usuario(alias, password, 1);  //RELLENA EL CONSTRUCTOR

                try {
                    if ((bbdd.getUsuario(alias).getAdmin()) == -1) {    //Comprobación de que el usuario no existe
                        
                        //GUARDA USUARIO EN LA BBDD
                        bbdd.addUsuario(usu);   //METODO PARA AÑADIR USUARIO A LA BBDD
                        JOptionPane.showMessageDialog(null, "Usuario creado correctamente", "Registro", JOptionPane.INFORMATION_MESSAGE);
                        FramePrincipal.crearFramePrincipal();   //TE LLEVA A LA VENTANA PRINCIPAL
                        registrarFrame.setVisible(false);
                        loginFrame.setVisible(false);
                    } else {
                        JOptionPane.showMessageDialog(null, "Usuario existente", "Registro", JOptionPane.ERROR_MESSAGE);
                    }

                } catch (ApuestasException ex) {
                    ex.getStackTrace();
                }

            } else {
                JOptionPane.showMessageDialog(null, "Las contraseñas no coinciden", "Registro", JOptionPane.ERROR_MESSAGE);
            }
        } else{
            JOptionPane.showMessageDialog(null, "Rellene todos los campos", "Registro", JOptionPane.ERROR_MESSAGE);
        }
    }

}
