/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.entidades;

/**
 *
 * @author Grupo 1 Java
 */
public class Usuario {
    private String alias;
    private String password;
    private int admin;

    public Usuario() {
    }

    public Usuario(String alias, String password, int admin) {
        this.alias = alias;
        this.password = password;
        this.admin = admin;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }
    
    
}
