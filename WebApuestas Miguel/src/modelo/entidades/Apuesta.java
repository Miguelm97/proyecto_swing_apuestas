/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.entidades;

/**
 *
 * @author Miguel
 */
public class Apuesta {
    private int idApuesta;
    private String deporte;
    private String fechaPartido;
    private String eqLocal;
    private String eqVisitante;
    private String mercado;
    private double cuota;
    private int acertada;

    public Apuesta(int idApuesta, String deporte, String fechaPartido, String eqLocal, String eqVisitante, String mercado, double cuota, int acertada) {
        this.idApuesta = idApuesta;
        this.deporte = deporte;
        this.fechaPartido = fechaPartido;
        this.eqLocal = eqLocal;
        this.eqVisitante = eqVisitante;
        this.mercado = mercado;
        this.cuota = cuota;
        this.acertada = acertada;
    }

    

    public int getIdApuesta() {
        return idApuesta;
    }

    public void setIdApuesta(int idApuesta) {
        this.idApuesta = idApuesta;
    }

    public String getDeporte() {
        return deporte;
    }

    public void setDeporte(String deporte) {
        this.deporte = deporte;
    }

    public String getFechaPartido() {
        return fechaPartido;
    }

    public void setFechaPartido(String fechaPartido) {
        this.fechaPartido = fechaPartido;
    }

    public String getEqLocal() {
        return eqLocal;
    }

    public void setEqLocal(String eqLocal) {
        this.eqLocal = eqLocal;
    }

    public String getEqVisitante() {
        return eqVisitante;
    }

    public void setEqVisitante(String eqVisitante) {
        this.eqVisitante = eqVisitante;
    }

    public String getMercado() {
        return mercado;
    }

    public void setMercado(String mercado) {
        this.mercado = mercado;
    }

    public double getCuota() {
        return cuota;
    }

    public void setCuota(double cuota) {
        this.cuota = cuota;
    }

    public int getAcertada() {
        return acertada;
    }

    public void setAcertada(int acertada) {
        this.acertada = acertada;
    }
    
    

    @Override
    public String toString() {
        return "Apuesta{" + "idApuesta=" + idApuesta + ", deporte=" + deporte + ", fechaPartido=" + fechaPartido + ", eqLocal=" + eqLocal + ", eqVisitante=" + eqVisitante + ", mercado=" + mercado + ", cuota=" + cuota + '}';
    }
    
    
}
