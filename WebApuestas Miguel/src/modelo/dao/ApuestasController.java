package modelo.dao;

import excepciones.ApuestasException;
import java.util.List;
import modelo.entidades.Apuesta;
import modelo.entidades.Usuario;


/**
 *
 * @author Grupo 1 Java
 */
public interface ApuestasController {
    public abstract void addApuesta(Apuesta apu) throws ApuestasException;
    public abstract List getApuestas() throws ApuestasException;
    public abstract void deleteApuesta(int id) throws ApuestasException;
    public abstract Apuesta getApuesta(int id) throws ApuestasException;
    public abstract void editApuesta(Apuesta apu) throws ApuestasException;
    public abstract void resultadoApuesta(int id, int resultado) throws ApuestasException;
}
