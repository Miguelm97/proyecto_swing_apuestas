/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import excepciones.ApuestasException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import modelo.entidades.Apuesta;
import modelo.entidades.Usuario;
import utilidades.ConexionBBDD;

/**
 *
 * @author Grupo 1 Java
 */
public class ApuestasControlerImpl implements ApuestasController {

    private Connection getConection() throws SQLException {
        return DriverManager.getConnection(ConexionBBDD.URL, ConexionBBDD.USER, ConexionBBDD.PASSWORD);
    }

    
                                            //GESTIÓN DE APUESTAS CON BBDD
    
    @Override
    public void addApuesta(Apuesta apu) throws ApuestasException {  //AÑADIR APUESTA A LA BBDD

        try {
            Connection conexion = getConection();
            Statement st = conexion.createStatement();
            
            st.executeUpdate("INSERT INTO web_apuestas.apuesta(deporte, fecha_partido, local, visitante, mercado, cuota) " +
                "VALUES('" + apu.getDeporte() + "','" + apu.getFechaPartido() + "','" + apu.getEqLocal() + "','" + apu.getEqVisitante() + "','" + 
                    apu.getMercado() + "'," + apu.getCuota() + ");");

            conexion.close();
        } catch (SQLException ex) {
            ex.getStackTrace();
        }

    }
    
    
    @Override
    public List getApuestas() throws ApuestasException {    //OBTENER UN LISTADO DE APUESTAS DE LA BASE DE DATOS

        List<Apuesta> listaApuestas = new ArrayList<>();
        try {

            Connection conexion = getConection();

            Statement st = conexion.createStatement();

            ResultSet rs = st.executeQuery("select id_apuesta, deporte, fecha_partido, local, visitante, mercado, cuota, acertada from web_apuestas.apuesta;");

            Apuesta apu;
            while (rs.next()) {
                apu = new Apuesta(rs.getInt("id_apuesta"), rs.getString("deporte"), rs.getString("fecha_partido"), rs.getString("local"), rs.getString("visitante"), rs.getString("mercado"), rs.getDouble("cuota"), rs.getInt("acertada"));
                listaApuestas.add(apu);
            }

            conexion.close();

        } catch (SQLException ex) {
            throw new ApuestasException("Error getUsuarios. Razon: " + ex.getMessage());
        }
        return listaApuestas;
    }
    
    @Override
    public void deleteApuesta(int id) throws ApuestasException {    //ELIMINAR UNA APUESTA DE LA BASE DE DATOS
        try {
            // Crear objeto para conectar con servidor
            Connection conexion = getConection();

            // Crear objeto Statement
            Statement st = conexion.createStatement();
            
            // Ejecutar instruccion DELETE
            st.executeUpdate("DELETE FROM web_apuestas.apuesta WHERE id_apuesta = " + id);

            // Cerrar conexion
            conexion.close();
            
        } catch (SQLException ex) {
            // Crear y lanzar una excepcion personalizada indicando como mensaje
            // el de SQL generado
            throw new ApuestasException("Error delete. Razon " + ex.getMessage());
        }
    }
    
    @Override
    public Apuesta getApuesta(int id) throws ApuestasException {    //OBTENER UNA APUESTA DE LA BBDD
        Apuesta apuesta = null;
        try {
            Connection conexion = getConection();
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("select deporte, fecha_partido, local, visitante, mercado, cuota, acertada from web_apuestas.apuesta where id_apuesta = '" + id + "';");
            
            rs.next();
            apuesta = new Apuesta(id, rs.getString("deporte"), rs.getString("fecha_partido"), rs.getString("local"), rs.getString("visitante"), rs.getString("mercado"), rs.getDouble("cuota"), rs.getInt("acertada"));


            conexion.close();

        } catch (SQLException ex) {
            throw new ApuestasException("Error getUsuarios. Razon: " + ex.getMessage());
        }

        return apuesta;
    }
    
    @Override
    public void editApuesta(Apuesta apu) throws ApuestasException {

        try {
            Connection conexion = getConection();
            Statement st = conexion.createStatement();

            st.executeUpdate("UPDATE web_apuestas.apuesta "
                    + "SET deporte = '" + apu.getDeporte() + "', fecha_partido = '" + apu.getFechaPartido() + "', "
                            + "local = '" + apu.getEqLocal() + "',visitante = '" + apu.getEqVisitante() + "', "
                                    + "mercado = '" + apu.getMercado() + "', cuota = " + apu.getCuota()
                                            + " WHERE id_apuesta = " + apu.getIdApuesta() + ";");

            conexion.close();
        } catch (SQLException ex) {
            ex.getStackTrace();
        }

    }
    
    @Override
    public void resultadoApuesta(int id, int resultado) throws ApuestasException {

        try {
            Connection conexion = getConection();
            Statement st = conexion.createStatement();

            st.executeUpdate("UPDATE web_apuestas.apuesta "
                    + "SET acertada = " + resultado + " WHERE id_apuesta = " + id + ";");

            conexion.close();
        } catch (SQLException ex) {
            ex.getStackTrace();
        }

    }
    
}
