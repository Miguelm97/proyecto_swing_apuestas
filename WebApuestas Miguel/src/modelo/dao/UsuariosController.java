package modelo.dao;

import excepciones.ApuestasException;
import java.util.List;
import modelo.entidades.Usuario;


/**
 *
 * @author Grupo 1 Java
 */
public interface UsuariosController {
    public abstract void addUsuario(Usuario u) throws ApuestasException;
    public abstract Usuario getUsuario(String alias) throws ApuestasException;
    public abstract void actualizarSaldo(int idApu) throws ApuestasException;
}
