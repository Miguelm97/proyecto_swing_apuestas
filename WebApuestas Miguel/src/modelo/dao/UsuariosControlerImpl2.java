/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import excepciones.ApuestasException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import modelo.entidades.Apuesta;
import modelo.entidades.Usuario;
import utilidades.ConexionBBDD;

/**
 *
 * @author Grupo 1 Java
 */
public class UsuariosControlerImpl2 implements UsuariosController {

    private Connection getConection() throws SQLException {
        return DriverManager.getConnection(ConexionBBDD.URL, ConexionBBDD.USER, ConexionBBDD.PASSWORD);
    }

                                            //GESTIÓN DE USUARIOS CON BBDD
    @Override
    public void addUsuario(Usuario usu) throws ApuestasException {  //AÑADE UN USUARIO DE LA BBDD

        try {
            Connection conexion = getConection();
            Statement st = conexion.createStatement();
            st.executeUpdate("INSERT INTO web_apuestas.usuario "
                    + "(alias, password, admin) "
                    + "VALUES(\"" + usu.getAlias() + "\",\"" + usu.getPassword() + "\"," + usu.getAdmin() + ");");

            conexion.close();
        } catch (SQLException ex) {
            ex.getStackTrace();
        }

    }

    @Override
    public Usuario getUsuario(String alias) throws ApuestasException {  //BUSCA POR EL ALIAS UN USUARIO DE LA BBDD
        Usuario usuario = null;
        try {

            Connection conexion = getConection();

            Statement st = conexion.createStatement();

            ResultSet rs = st.executeQuery("select alias, password, admin from  web_apuestas.usuario where alias = '" + alias + "';");
            
            if (rs.next()) {    //COMPRUEBO SI EXISTE EL USUARIO CON DICHO ALIAS
                usuario = new Usuario(rs.getString("Alias"), rs.getString("Password"), rs.getInt("Admin")); //Usuario SI existe
            } else {
                usuario = new Usuario("", "", -1); //Admin -1 --> Usuario NO existe
            }

            conexion.close();

        } catch (SQLException ex) {
            throw new ApuestasException("Error getUsuarios. Razon: " + ex.getMessage());
        }

        return usuario;
    }
    
    @Override
    public void actualizarSaldo(int idApu) throws ApuestasException{
        try{
            Connection conexion = getConection();
            Statement st = conexion.createStatement();
            ResultSet rs;
            rs = st.executeQuery("select cuota from apuesta where id_apuesta = " + idApu + ";");
            rs.next();
            double cuota = rs.getDouble("cuota");
            System.out.println("cuota: " + cuota);
            rs = st.executeQuery("select ua.id_usuario, ua.apostado from "
                    + "usuario u, usuario_apuesta ua where u.id_usuario = ua.id_usuario and ua.id_apuesta = " + idApu + ";");

            PreparedStatement ps = conexion.prepareStatement("UPDATE web_apuestas.usuario SET saldo = saldo+? WHERE id_usuario = ?;");

            while (rs.next()) {
                ps.setDouble(1, rs.getDouble("apostado")*cuota);
                ps.setInt(2, rs.getInt("id_usuario"));
                
                System.out.println("id_usuario=" + rs.getInt("id_usuario") + ", apostado=" + rs.getDouble("apostado")*cuota);
                rs = ps.executeQuery();     // Ejecutar instruccion
            }


            conexion.close();
        } catch(SQLException ex){
            throw new ApuestasException("Error actualizarSaldo. Razon: " + ex.getMessage());
        }
    }

    
    /*
    public List getUsuarios() throws ApuestasException {

        List<Usuario> lC = new ArrayList<>();
        try {

            Connection conexion = getConection();

            Statement st = conexion.createStatement();

            ResultSet rs = st.executeQuery("select alias, password, admin from  web_apuestas.usuario;");

            Usuario usuario;
            while (rs.next()) {
                usuario = new Usuario(rs.getString("Alias"), rs.getString("Password"), rs.getInt("Admin"));
                lC.add(usuario);
            }

            conexion.close();

        } catch (SQLException ex) {
            throw new ApuestasException("Error getUsuarios. Razon: " + ex.getMessage());
        }
        return lC;
    }
*/

}
